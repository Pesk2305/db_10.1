import psycopg2
from psycopg2.extras import RealDictCursor

conn = psycopg2.connect("""
password=postgres dbname=postgres user=postgres port=38746 host=localhost
""", cursor_factory=RealDictCursor)


class Cars:
    def __init__(self, id: int, name: str, date_of_manufacture: int, rating: int, review: str, driving_style: str, quantity: int, fuel: str):
        self.id = id
        self.name = name
        self.date_of_manufacture = date_of_manufacture
        self.rating = rating
        self.review = review
        self.driving_style = driving_style
        self.quantity = quantity
        self.fuel = fuel

    def get_info(self):
        return f"""ID: {self.id}, Марка: {self.name}, Дата создания: {self.date_of_manufacture}, Рейтинг: {self.rating}, Описание: {self.review}, Стиль вождения: {self.driving_style}, Количество экземпляров: {self.quantity}, Тип топлива:"{self.fuel} """


class Cooperator:
    def __init__(self, id: int, function: str, wage: int, prize: int):
        self.id = id
        self.function = function
        self.wage = wage
        self.prize = prize

    def get_info(self):
        return f""" ID: {self.id}, Должность: {self.function}, ЗП: {self.wage}p, Размер премии: {self.prize}p"""


class Service:
    def __init__(self, id: int, name: str, founding_date: int, quality: str, place: str):
        self.id = id
        self.name = name
        self.founding_date = founding_date
        self.quality = quality
        self.place = place
        self.car = []
        self.service_cooperators = []

    def show_info(self):
        print(f"""ID: {self.id}, Название: {self.name}, Дата основания: {self.founding_date}, Качество производства: {self.quality}, Глав. офис: {self.place}""")

        if len(self.car):
            print("    Машины:")
            for c in self.car:
                print("      " + c.get_info())

        if len(self.service_cooperators):
            print("    Сотрудники:")
            for sc in self.service_cooperators:
                print("      " + sc.get_info())


def print_service():
    cur = conn.cursor()

    cur.execute("""
select s.id service_id,
    s.name_s,
    s.founding_date,
    s.quality,
    s.place,
    sc.id cooperator_id,
    sc.function,
    sc.wage,
    sc.prize,
    c.id cars_id,
    c.name,
    c.date_of_manufacture,
    c.rating,
    c.review,
    c.driving_style,
    c.quantity,
    c.fuel  
from service s
    join service_cooperator sc on s.id = sc.service_id
    join cars_to_service cs on s.id = cs.service_id
    join cars c on c.id = cs.cars_id;
""")

    rows = cur.fetchall()
    service_dict = {}
    cooperator_dict = {}
    car_dict = {}
    for row in rows:
        service_id = row['service_id']
        if service_id not in service_dict:
            service = Service(row['service_id'], row['name_s'], row['founding_date'], row['quality'], row['place'])
            service_dict[service_id] = service
        else:
            service = service_dict[service_id]

        cooperator_id = row['cooperator_id']
        if cooperator_id is not None and cooperator_id not in cooperator_dict:
            cooperator = Cooperator(row['cooperator_id'], row['function'], row['wage'], row['prize'])
            cooperator_dict[cooperator_id] = cooperator
        elif cooperator_id is not None:
            cooperator = cooperator_dict[cooperator_id]

        if cooperator_id is not None and cooperator not in service.service_cooperators:
            service.service_cooperators.append(cooperator)

        car_id = row['cars_id']
        if car_id is not None and car_id not in car_dict:
            car = Cars(car_id, row['name'], row['date_of_manufacture'], row['rating'], row['review'], row['driving_style'], row['quantity'], row['fuel'])
            car_dict[car_id] = car
        elif car_id is not None:
            car = car_dict[car_id]

        if car_id is not None and car not in service.car:
            service.car.append(car)

    for service in service_dict.values():
        service.show_info()


print_service()
