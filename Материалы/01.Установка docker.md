```shell
# set up the repository https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install docker engine
sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Даем права для запуска
```shell
sudo groupadd docker
```
```shell
sudo usermod -aG docker $USER
```
```shell
newgrp docker
```

Меняем подсеть докера, чтобы она не конфликтовала с адресом сервера авторизации wi-fi Сириуса. Иначе не будет работать интернет.
```shell
echo '{
  "live-restore": true,
  "bip": "172.20.0.1/16",
  "default-address-pools": [{
    "base": "172.20.0.0/8",
    "size": 16
  }]
}
' | sudo tee /etc/docker/daemon.json

# важная строка "bip": "172.20.0.1/16" - это смена подсети докера

# после этого перезапускаем службу докера и проверяем настройки
# там должен появиться диапазон 172.20.0.0/16
sudo service docker restart
docker network inspect bridge
```

Ребутим, проверяем что все работает после ребута **без(!) рут прав**
```shell
docker run hello-world
```
